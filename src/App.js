import React from "react";
import axios from "axios";
import "./App.css";
import moment from "moment";
import {ConfirmModal} from "./component/modal-confirm"
import {ConfirmModalAdd} from "./component/modal-add"
import { Form, Input, DatePicker } from 'antd';

import { Button,Row, Col } from "antd";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      posts: [],
      id: '',
      modalDelete: false,
      modaladd: false,
      name:'',
      email:'',
      date: ''
    };
  }

  handleFetchUsers = () => {
    axios
      .get("https://5d60ae24c2ca490014b27087.mockapi.io/api/v1/users")
      .then(({ data }) => {
        this.setState({
          users : data
        });
      });
  };

  handleFetchPosts = () => {
    axios
      .get("https://5d60ae24c2ca490014b27087.mockapi.io/api/v1/photos")
      .then(({ data }) => {
        this.setState({
          posts : data
        });
      });
  };

  handleFecthAll = () => {
    Promise.all([
      axios
      .get("https://5d60ae24c2ca490014b27087.mockapi.io/api/v1/photos")
      .then(({data}) =>  data),
      axios
      .get("https://5d60ae24c2ca490014b27087.mockapi.io/api/v1/users")
      .then(({data}) => data),
    ]).then(data =>{
      this.setState({
        posts : data[0],
        users : data[1]
      });
      console.log(data)
    })
  };

  handleOpenModal = (id) => {
    this.setState({
      modalDelete: !this.state.modalDelete,
      id,
    });
  };

  handleOk = () => {
    console.log(this.state.id)
    const {id} = this.state;
    this.handleOpenModal(id)
    axios
    .delete(`https://5d60ae24c2ca490014b27087.mockapi.io/api/v1/users/${id}`)
    .then(() =>{
      alert("Berhasil simpan!")
      this.handleFecthAll()
    })
    .catch(err => {
      console.error("err", err)
    }

    )
  };

  handleOpenModalAdd = () => {
    this.setState({
      modalAdd: !this.state.modalAdd
    });
  };

  handleOkAdd = () => {
    this.handleOpenModalAdd()
    axios.post('https://5d60ae24c2ca490014b27087.mockapi.io/api/v1/users', {
      name: this.state.name, 
      email: this.state.email,
      createdAt: moment(this.state.date).format()})
        .then(function (response) {
            console.log(response);
            alert("Berhasil simpan!")
            this.handleFecthAll()
        })
        .catch(function (error) {
            console.log(error);
    });
  };

  handleOnChange = e =>{
    console.log('value', e.target.value)
    const {inputType} = e.currentTarget.dataset
    this.setState({
      [inputType]: e.target.value
    })
  }
  onChange = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      date
    })
  }
  render() {
    return (
      <div style={{margin:50}}>
        <ConfirmModal 
        title="Confirm Modal Delete"
        visible={this.state.modalDelete}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        >
          Apakah anda yakin ?
        </ConfirmModal>
        
        <ConfirmModalAdd 
        title="Confirm Modal Add"
        visible={this.state.modalAdd}
        onOk={this.handleOkAdd}
        onCancel={this.handleCancel}
        >
          <Form onSubmit={this.handleSubmit}>
        <Form.Item label='Name' >
            <Input
              data-input-type="name"
              value={this.state.name}
              onChange={(e) => this.handleOnChange(e)}
              placeholder="Name"
            />
        </Form.Item>
        <Form.Item label="Email">
            <Input
              data-input-type="email"
              value={this.state.email}
              onChange={(e) => this.handleOnChange(e)}
              placeholder="Email"
            />
        </Form.Item>
      <Form.Item label="Created At">
            <DatePicker
              onChange={this.onChange}
              placeholder="Created at"
            />
        </Form.Item>
      </Form>
        </ConfirmModalAdd>
    <Row>
      <Col span={12}>
        <div>
          <Button onClick={() => this.handleOpenModalAdd()} type="primary">
            add user
          </Button>
          
          <thead className="ant-table-thead">
            <tr>
              <th className="ant-table-header-column">Created At</th>
              <th className="ant-table-header-column">Photos</th>
              <th className="ant-table-header-column">Count Likes</th>
            </tr>
          </thead>
          <tbody>
            {this.state.posts.map(v => (
              <tr key={v.id}>
              <td>{moment(v.createdAt).format('MMMM Do YYYY, h:mm:ss a')}</td>
              <td><img src={v.photos} alt={v.photos} style={{width:100, height:100}}/></td>
              <td>{v.count_likes}</td>
              </tr>
            ))}
          </tbody>
          
        </div>
      </Col>
      <Col span={12}>
        <div>
          <Button onClick={this.handleFecthAll} type="primary">
            Fetch All
          </Button>          
          <thead className="ant-table-thead">
            <tr>
              <th className="ant-table-header-column">Created At</th>
              <th className="ant-table-header-column">Name</th>
              <th className="ant-table-header-column">Avatar</th>
              <th className="ant-table-header-column">Action</th>
            </tr>
          </thead>
          <tbody>
            {this.state.users.map(v => (
              <tr key={v.id}>
              <td>{moment(v.createdAt).format('MMMM Do YYYY, h:mm:ss a')}</td>
              <td>{v.name}</td>
              <td><img src={v.avatar} alt={v.avatar} style={{width:100, height:100}}/></td>
              <td><Button onClick={() => this.handleOpenModal(v.id)} >Delete</Button></td>
              </tr>
            ))}
          </tbody>
          
        </div>
      </Col>
    </Row>
      </div>
    );
  }
}

export default App;
